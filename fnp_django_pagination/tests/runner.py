#!/usr/bin/env python

import django
import os
import sys

from django.core.management import call_command


def runtests():
    os.environ['DJANGO_SETTINGS_MODULE'] = 'fnp_django_pagination.tests.settings'
    django.setup()

    failures = call_command('test', 'fnp_django_pagination')
    sys.exit(bool(failures))

if __name__ == '__main__':
    runtests()
